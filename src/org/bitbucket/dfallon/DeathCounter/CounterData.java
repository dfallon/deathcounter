package org.bitbucket.dfallon.DeathCounter;

import java.util.HashSet;
import java.util.Iterator;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;

public class CounterData {
	// data stores
	private ConfigAccessor playerList;

	// otherinfo

	private ConfigAccessor signList;
	// return sign from config string
	public static Sign getSign(String s) {
		String[] tokens = s.split("/");
		// get world
		World world = DeathCounter.plugin.getServer().getWorld(tokens[0]);
		// get block
		Block block = world.getBlockAt(Integer.parseInt(tokens[1]),
				Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]));
		if (block.getState() instanceof Sign)
			return (Sign) block.getState();
		// return block
		return null;
	}
	public CounterData() {
		// open config files
		playerList = new ConfigAccessor(DeathCounter.plugin, "players.yml");
		signList = new ConfigAccessor(DeathCounter.plugin, "signs.yml");
	}
	public void addPlayer(String playerName) {
		playerName = playerName.toLowerCase();
		playerList.getConfig().set(playerName + ".count", 0);
		playerList.saveConfig();
	}
	
	// Sign methods
	// add sign to config
	public void addSign(SignChangeEvent e) {
		Block s = e.getBlock();
		// create sign key world/x/y/z
		String signKey = s.getWorld().getName() + "/" + s.getX() + "/" + s.getY() + "/"
				+ s.getZ();
		signList.getConfig().set(signKey, e.getLine(0).toLowerCase());
		signList.saveConfig();
		//add death count
		e.setLine(2, Integer.toString(getDeathCount(e.getLine(0))));
	}
	
	// player methods
	public int getDeathCount(String playerName) {
		playerName = playerName.toLowerCase();
		Object playerData = playerList.getConfig().get(playerName + ".count");
		if (playerData instanceof Integer) {
			return (int) playerData;
		} else {
			addPlayer(playerName);
			return getDeathCount(playerName);
		}
	}
	
	// searching methods
	private Set<Sign> getSignList() {
		Iterator<String> signs = signList.getConfig().getKeys(false).iterator();
		Set<Sign> signBlocks = new HashSet<Sign>();

		// iterate through signs
		while (signs.hasNext()) {
			// get sign from world and add to sign block
			Sign s = getSign(signs.next());
			if (s != null)
				signBlocks.add(s);
			else
				DeathCounter.plugin.getLogger().log(Level.WARNING,
						"no sign found at location");
			signs.remove();
		}
		return signBlocks;
	}
	
	private Set<Sign> getSignList(String playerName) {
		playerName = playerName.toLowerCase();
		Iterator<Entry<String, Object>> signs = signList.getConfig()
				.getValues(false).entrySet().iterator();
		Set<Sign> signBlocks = new HashSet<Sign>();

		// iterate through signs to find those with the player.
		while (signs.hasNext()) {
			Map.Entry<String, Object> sign = signs.next();
			if ((sign.getValue() instanceof String)
					&& playerName.equals((String)sign.getValue())) {
				// get sign from key
				Sign s = getSign(sign.getKey());
				if (s != null)
					signBlocks.add(s);
			}
		}
		return signBlocks;
	}
	
	public void incrementDeathCount(Player p){
		String playerName = p.getName().toLowerCase();
		int newDeathCount = getDeathCount(playerName)+1;
		setDeathCount(playerName,newDeathCount);
		updateSigns(playerName);
	}

	public Boolean isDeathCountSign(Sign s){
		String signKey = s.getWorld().getName()+"/"+s.getX()+"/"+s.getY()+"/"+s.getZ();
		//check if key returns data
		return signList.getConfig().get(signKey) != null;
	}
	public void removePlayer(String playerName){
		playerList.getConfig().set(playerName, null);
		playerList.saveConfig();
	}
	
	public void removeSign(Sign s) {
		String signKey = s.getWorld().getName() + "/" + s.getX() + "/" + s.getY() + "/"
				+ s.getZ();
		signList.getConfig().set(signKey, null);
		signList.saveConfig();
	}

	public void setDeathCount(String playerName, int numDeaths) {
		playerList.getConfig().set(playerName.toLowerCase() + ".count", numDeaths);
		playerList.saveConfig();
	}

	public void updateSign(Sign s) {
		String playerName = s.getLine(0).toLowerCase();
		s.setLine(2, Integer.toString(getDeathCount(playerName)));
		s.update();
	}
	public void updateSigns(String playerName){
		Iterator<Sign> signs =  getSignList(playerName).iterator();
		while(signs.hasNext()){
			Sign s = signs.next();
			updateSign(s);
		}
	}

}
