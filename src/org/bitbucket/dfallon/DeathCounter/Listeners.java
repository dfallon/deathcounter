package org.bitbucket.dfallon.DeathCounter;

import java.util.logging.Level;

import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Listeners implements Listener {
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		if((event.getBlock().getState() instanceof Sign) && DeathCounter.data.isDeathCountSign((Sign)event.getBlock().getState()))
			DeathCounter.data.removeSign((Sign)event.getBlock().getState());
	}
	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		if (DeathCounter.data.isDeathCountSign((Sign) event.getBlock().getState()))
			event.setCancelled(true);
		
		else if (event.getLine(1).equalsIgnoreCase("[Deaths:]")) {
			DeathCounter.plugin.getLogger().log(Level.INFO, "Adding a sign");
			DeathCounter.data.addSign(event);
		}else{
			DeathCounter.plugin.getLogger().log(Level.INFO, ((Sign) event.getBlock().getState()).getLine(0));
			DeathCounter.plugin.getLogger().log(Level.INFO, ((Sign) event.getBlock().getState()).getLine(1));
			DeathCounter.plugin.getLogger().log(Level.INFO, ((Sign) event.getBlock().getState()).getLine(2));
			DeathCounter.plugin.getLogger().log(Level.INFO, "no sign added");
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent event){
		DeathCounter.data.incrementDeathCount(event.getEntity());
	}
}
