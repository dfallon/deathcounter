package org.bitbucket.dfallon.DeathCounter;

import java.util.Comparator;
import java.util.Map;

public class ValueComparator<T extends Comparable<T>> implements Comparator<String> {

	private Map<String, T > base;
	
	public ValueComparator(Map<String, T> base){
		this.base = base;
	}

	@Override
	public int compare(String o1, String o2) {
		int valuesCompared = base.get(o1).compareTo(base.get(o2));
		if(valuesCompared != 0)
			return valuesCompared;
		else
			return o1.compareTo(o2);
	
	}


}
