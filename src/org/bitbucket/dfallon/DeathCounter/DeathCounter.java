package org.bitbucket.dfallon.DeathCounter;

import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class DeathCounter extends JavaPlugin {
	
	protected static CounterData data;
	protected static JavaPlugin plugin;
	@Override
	public final void onEnable() {
		getLogger().log(Level.INFO, "DeathCounter has loaded");
		plugin = this;
		data = new CounterData();
		Listeners listeners = new Listeners();
		getServer().getPluginManager().registerEvents(listeners, this);
	}

	@Override
	public final void onDisable() {
		getLogger().log(Level.INFO,"DeathCounter has been unloaded");
		
	}
	
	//custom config methods
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(args.length<1)
			return false;
		if(args[0].equalsIgnoreCase("set")&&args.length>2&&sender.hasPermission("DeathCounter.set")){
				data.setDeathCount(args[1], Integer.parseInt(args[2]));
				sender.sendMessage(args[1]+"'s death coutn was set to: "+data.getDeathCount(args[1]));
				return true;
			}else if(args[0].equalsIgnoreCase("get")&&args.length>1&&sender.hasPermission("DeathCounter.get")){
				sender.sendMessage(args[1]+"'s death count is: "+data.getDeathCount(args[1]));
				return true;
			}else if(args[0].equalsIgnoreCase("top")&&sender.hasPermission("DeathCounter.score")){
				sender.sendMessage(data.getScoreboard());
			}
		
		
		
		return false;
	}
	
}
